package com.leyou.item.controller;

import com.leyou.common.pojo.PageResult;
import com.leyou.item.service.GoodsService;
import com.leyou.pojo.Sku;
import com.leyou.pojo.Spu;
import com.leyou.pojo.SpuBo;
import com.leyou.pojo.SpuDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class GoodsController {
    @Autowired
    private GoodsService goodsService;


    /**
     * 商品上下架功能完成
     * @Author Allen-fanxin
     * @Date 2020/9/28 10:06
     * @params [id]
     * @Since version-1.0
     */
    @PutMapping("goods/spu/out/{id}")
    public ResponseEntity<Void> goodsSoldOut(@PathVariable("id")Long id){
        this.goodsService.goodsSoldOut(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("sku/list")
    public ResponseEntity<List<Sku>> querySpusById(@RequestParam("id")Long spuId){
        List<Sku> spuList=this.goodsService.queryBySkuId(spuId);
        if (spuList==null || CollectionUtils.isEmpty(spuList)){
            return ResponseEntity.badRequest().build();
        }return ResponseEntity.ok(spuList);
    }

    /**
     * 新增商品
     * @Author Arrogant-zxx
     * @Date 2020/9/26 0026 15:46
     * @params [spuBo]
     * @Since version-1.0
     */
    @PostMapping("goods")
    public ResponseEntity<Void> saveGoods(@RequestBody SpuBo spuBo){
        this.goodsService.saveGoods(spuBo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /*
    * 修改商品接口
    * */
    @PutMapping("goods")
    public ResponseEntity<Void> updateGoods(@RequestBody SpuBo spuBo){
        this.goodsService.updateGoods(spuBo);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    /**
     * 分页查询
     * @Author Arrogant-zxx
     * @Date 2020/9/26 0026 0:14
     * @params [key, saleable, page, rows]
     * @Since version-1.0
     */
    @GetMapping("spu/page")
    public ResponseEntity<PageResult<SpuBo>> querySpuBoByPage(
            @RequestParam(value = "key", required = false)String key,
            @RequestParam(value = "saleable", required = false)Boolean saleable,
            @RequestParam(value = "page", defaultValue = "1")Integer page,
            @RequestParam(value = "rows", defaultValue = "5")Integer rows){
        PageResult<SpuBo> pageResult = this.goodsService.querySpuBoByPage(key, saleable, page, rows);
        if(CollectionUtils.isEmpty(pageResult.getItems())){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(pageResult);
    }
    /**
     * 删除商品列表
     * @Author Arrogant-zxx
     * @Date 2020/9/26 0026 0:01
     * @params [id]
     * @Since version-1.0
     */
    @DeleteMapping("spu/{id}")
    public ResponseEntity<Void> deleteGoods(@PathVariable("id") Long id){
        if(id<1){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else if (this.goodsService.deleteGoodsById(id)==1){
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 查询spudetail接口资源
     * @Author Arrogant-zxx
     * @Date 2020/9/26 0026 0:02
     * @params [spuId]
     * @Since version-1.0
     */
    @GetMapping("spu/detail/{spuId}")
    public ResponseEntity<SpuDetail> querySpuDetailBySpuId(@PathVariable("spuId")Long spuId){
        SpuDetail spuDetail = this.goodsService.querySpuDetailBySpuId(spuId);
        if (spuDetail == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(spuDetail);
    }
}
