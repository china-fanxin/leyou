package com.leyou.item.mapper;


import com.leyou.pojo.Brand;
import com.leyou.pojo.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BrandMapper extends Mapper<Brand> {

    @Select("SELECT * FROM tb_category WHERE id in (SELECT category_id FROM tb_category_brand WHERE brand_id=#{bid})")
    List<Category> queryCategoryByBid(Long bid);

    @Insert("INSERT INTO tb_category_brand(category_id,brand_id) VALUES(#{cid},#{bid}) ")
    int insertCategoryAndBrand(@Param("cid") Long cid, @Param("bid") Long id);

    @Select("SELECT * FROM tb_brand a INNER JOIN tb_category_brand b on a.id=b.brand_id where b.category_id=#{cid}")
    List<Brand> selectBrandsByCid(Long cid);

    @Delete("DELETE FROM tb_category_brand WHERE brand_id=#{id}")
    int deleteCategoryAndBrand(Long id);
}
