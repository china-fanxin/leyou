package com.leyou.item.controller;

import com.leyou.common.pojo.PageResult;
import com.leyou.item.service.BrandService;
import com.leyou.pojo.Brand;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 根据查询条件分页并排序查询品牌信息
     * @Author Allen-fanxin
     * @Date 2020/9/23 11:38
     * @params [key, page, rows, sortBy, desc]
     * @Since version-1.0
     */
    @GetMapping("page")
    public ResponseEntity<PageResult<Brand>> queryBrandsByPage(
            @RequestParam(value = "key",required = false)String key,
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value = "rows",defaultValue = "5")Integer rows,
            @RequestParam(value = "sortBy",required = false)String sortBy,
            @RequestParam(value = "desc",required = false)Boolean desc
    ){
        PageResult<Brand> result = this.brandService.queryBrandsByPage(key, page, rows, sortBy, desc);
        if (CollectionUtils.isEmpty(result.getItems())){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    /**
     * 新增品牌
     * @Author Allen-fanxin
     * @Date 2020/9/23 20:57
     * @params [brand, cids]
     * @Since version-1.0
     */
    @PostMapping
    public ResponseEntity<Void> saveBrand(Brand brand,@RequestParam("cids")List<Long>cids){
        System.out.println(brand.getImage());
        System.out.println(cids);
        this.brandService.saveBrand(brand,cids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("cid/{cid}")
    public ResponseEntity<List<Brand>> queryBrandsByCid(@PathVariable("cid")Long cid){
        List<Brand> brands= this.brandService.queryBrandsByCid(cid);
        if (CollectionUtils.isEmpty(brands)){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(brands);
    }
    /**
     * 修改品牌信息
     * @Author Allen-fanxin
     * @Date 2020/9/25 10:29
     * @params [brand]
     * @Since version-1.0
     */
    @PutMapping
    public ResponseEntity<Void> updateBrand(Brand brand){
        System.out.println(brand);
        if (StringUtils.isBlank(brand.toString())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else if(this.brandService.updateBrand(brand)==1){
            return ResponseEntity.status(HttpStatus.OK).build();
        }return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }

    /**
     * 根据id删除品牌信息
     * @Author Allen-fanxin
     * @Date 2020/9/25 12:16
     * @params [brand]
     * @Since version-1.0
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteBrand(@PathVariable Long id){
        System.out.println(id);
        if (id==null || id<=0){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        int i = this.brandService.deleteBrand(id);
        if (i!=0){
            return ResponseEntity.ok().build();
        }return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
