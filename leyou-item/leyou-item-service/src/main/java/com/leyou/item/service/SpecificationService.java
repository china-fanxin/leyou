package com.leyou.item.service;

import com.leyou.item.mapper.SpecGroupMapper;
import com.leyou.item.mapper.SpecParamMapper;
import com.leyou.pojo.SpecGroup;
import com.leyou.pojo.SpecParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecificationService {

    @Autowired
    private SpecGroupMapper specGroupMapper;

    public List<SpecGroup> queryGroupsByCid(Long cid){
        SpecGroup specGroup=new SpecGroup();
        specGroup.setCid(cid);
        return this.specGroupMapper.select(specGroup);
    }

    @Autowired
    private SpecParamMapper specParamMapper;

    public List<SpecParam> queryParams(Long gid){
        SpecParam specParam=new SpecParam();
        specParam.setGroupId(gid);
        return this.specParamMapper.select(specParam);
    }


    public void saveSpecGroup(SpecGroup specGroup){
        int count = this.specGroupMapper.insert(specGroup);
    }

    public void saveSpecParams(SpecParam specParam){
        this.specParamMapper.insert(specParam);
    }


    public void updateSpecGroup(SpecGroup specGroup){
        this.specGroupMapper.updateByPrimaryKeySelective(specGroup);
    }
    public void updateSpecParam(SpecParam specParam){
        this.specParamMapper.updateByPrimaryKeySelective(specParam);
    }

    public void deleteSpecGroup(Long id){
        this.specGroupMapper.deleteByPrimaryKey(id);
    }
    public void deleteSpecParam(Long id){
        this.specParamMapper.deleteByPrimaryKey(id);
    }

    public List<SpecParam> queryParams(Long gid, Long cid, Boolean generic, Boolean searching) {
        SpecParam record = new SpecParam();
        record.setGroupId(gid);
        record.setCid(cid);
        record.setGeneric(generic);
        record.setSearching(searching);
        return this.specParamMapper.select(record);
    }
}
