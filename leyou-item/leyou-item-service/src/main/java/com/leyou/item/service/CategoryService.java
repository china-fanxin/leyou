package com.leyou.item.service;

import com.leyou.item.mapper.CategoryMapper;
import com.leyou.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    private List<String> ids=new ArrayList<>();
    /**
     * 分局父节点查询子节点
     * @Author Allen-fanxin
     * @Date 2020/9/22 21:53
     * @params [pid]
     * @Since version-1.0
     */
    public List<Category> queryCategoriesByPid(Long pid){
        Category record = new Category();
        record.setParentId(pid);
        return this.categoryMapper.select(record);
    }

    public List<String> queryNamesByIds(List<Long> ids) {
        List<Category> list =this.categoryMapper.selectByIdList(ids);
        return list.stream().map(category -> category.getName()).collect(Collectors.toList());
    }
}
