package com.leyou.user.service;

import com.leyou.common.pojo.CodeCache;
import com.leyou.common.pojo.CodecUtils;
import com.leyou.common.pojo.NumberUtils;
import com.leyou.sms.utils.SendSms;
import com.leyou.user.mapper.UserMapper;
import com.leyou.user.pojo.User;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    Logger logger= LoggerFactory.getLogger(UserService.class);

    /**
     *
     * API 文档参数：
     * 实现用户数据的校验，主要包括对：手机号、用户名的唯一性校验。
     * **接口路径：GET /check/{data}/{type}
     * **参数说明：**
     *
     * | 参数 |说明|是否必须|数据类型|默认值|
     * | --- |---| -----| ------|-----|
     * |data|要校验的数据|是| String|无|
     * |type|要校验的数据类型：1，用户名；2，手机|否|Integer|1|
     * 返回布尔类型结果：
     * - true：可用
     * - false：不可用
     *
     * @Author Allen-fanxin
     * @Date 2020/9/26 11:40
     * @params [data, type]
     * @Since version-1.0
     */
    public Boolean checkData(String data, Integer type) {
        User record = new User();
        switch (type){
            case 1: record.setUsername(data); break;
            case 2: record.setPhone(data); break;
            default:return null;
        }
        return this.userMapper.selectCount(record)==0;
    }

    /**
     * 短信发送服务Service
     * @Author Allen-fanxin
     * @Date 2020/9/27 17:48
     * @params [phone]
     * @Since version-1.0
     */
    public Boolean sendVerifyCode(String phone) {
        // 生成验证码
        String code = NumberUtils.generateCode(6);
        CodeCache.addCache(code);
        System.out.println(CodeCache.getCache());
        try {
            // 发送短信
            Map<String, String> msg = new HashMap<>();
            msg.put("phone", phone);
            msg.put("code", CodeCache.getCache());
            SendSms.send(msg);
            return true;
        } catch (Exception e) {
            logger.error("发送短信失败。phone：{}， code：{}", phone, code);
            return false;
        }
    }


    /**
     * 注册功能业务实现
     * @Author Allen-fanxin
     * @Date 2020/9/27 19:01
     * @params [user, code]
     * @Since version-1.0
     */
    public Boolean register(User user, String code) {
        // 校验短信验证码
        String cacheCode = CodeCache.getCache();
        if (!StringUtils.equals(code, cacheCode)) {
            return false;
        }

        // 生成盐
        String salt = CodecUtils.generateSalt();
        user.setSalt(salt);

        // 对密码加密
        user.setPassword(CodecUtils.md5Hex(user.getPassword(), salt));

        // 强制设置不能指定的参数为null
        user.setId(null);
        user.setCreated(new Date());
        // 添加到数据库
        boolean b = this.userMapper.insertSelective(user)==1;
        if (b){
            // 注册成功，删除redis中的记录
            CodeCache.delCache();
        }
        return b;
    }

    public User queryUser(String username, String password) {
        User record = new User();
        record.setUsername(username);
        //根据用户名查询用户是否存在
        User user = userMapper.selectOne(record);
        if (user==null){
            logger.info("用户名不存在！");
            user=null;
        }
        //检验密码是否正确
        if(!StringUtils.equals(CodecUtils.md5Hex(password,user.getSalt()),user.getPassword())){
            logger.info("密码错误！");
            user=null;
        }
        return user;
    }
}
