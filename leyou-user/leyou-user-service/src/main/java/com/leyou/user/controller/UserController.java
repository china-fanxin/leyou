package com.leyou.user.controller;

import com.leyou.sms.utils.SendSms;
import com.leyou.user.pojo.User;
import com.leyou.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 用户数据验证功能实现，通过 API 文档完成功能的开发
     * @Author Allen-fanxin
     * @Date 2020/9/26 11:08
     * @params [data, type]
     * @Since version-1.0
     */
    @GetMapping("/check/{data}/{type}")
    public ResponseEntity<Boolean> checkUserData(@PathVariable("data")String data, @PathVariable("type")Integer type){
        System.out.println(data);
        System.out.println(type);
        Boolean boo=this.userService.checkData(data,type);
        if (boo==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok(boo);
    }

    /**
     * 实现发送短信的功能
     * @Author Allen-fanxin
     * @Date 2020/9/27 17:40
     * @params [phone]
     * @Since version-1.0
     */
    @PostMapping("send")
    public ResponseEntity<Void> sendVerifyCode(String phone) {
        Boolean boo = this.userService.sendVerifyCode(phone);
        if (boo == null || !boo) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    /**
     * 绑定注册功能模块
     * @Author Allen-fanxin
     * @Date 2020/9/27 19:01
     * @params [user, code]
     * @Since version-1.0
     */
    @PostMapping("register")
    public ResponseEntity<Void> register(User user, @RequestParam("code") String code) {

        Boolean boo = this.userService.register(user, code);
        if (boo == null || !boo) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * 完成用户登录功能的开发
     * @Author Allen-fanxin
     * @Date 2020/9/28 17:08
     * @params [username, password]
     * @Since version-1.0
     */
    @PostMapping("login")
    public ResponseEntity<User> login(@RequestParam("username")String username,@RequestParam("password")String password) {
        //通过接受到的用户名进行密码查询，并对密码重新进行盐值加密，与数据库内存储的密码进行比较，判断用户密码是否一致
        if (this.userService.queryUser(username, password) == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }
}
