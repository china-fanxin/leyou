package com.leyou.common.pojo;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;

public class CodecUtils {
    public static String md5Hex(String data,String salt) {
        if (salt==null || salt.isEmpty()) {
            salt = data.hashCode() + "";
        }
        return DigestUtils.md5Hex(salt + DigestUtils.md5Hex(data));
    }

    public static String shaHex(String data, String salt) {
        if (salt==null || salt.isEmpty()) {
            salt = data.hashCode() + "";
        }
        return DigestUtils.sha512Hex(salt + DigestUtils.sha512Hex(data));
    }

    public static String generateSalt(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
