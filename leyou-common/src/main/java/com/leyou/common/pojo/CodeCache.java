package com.leyou.common.pojo;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class CodeCache {

    /*定义HashMap当作缓存*/
    private static HashMap<String, String> codeList = new HashMap<>();

    /*保存时间*/
    private static int liveTime = 50;

    /*添加缓存*/
    public static boolean addCache(String code){
        codeList.put("code",code);
        return true;
    }

    /*删除缓存信息*/
    public static boolean delCache(){
        codeList.remove("code");
        return true;
    }

    /* 获取缓存信息*/
    public static String getCache(){
        return codeList.get("code");
    }

}
